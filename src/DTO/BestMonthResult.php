<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

class BestMonthResult
{
    /**
     * @Groups({"apiV1"})
     *
     * @var int
     */
    private $month;
    /**
     * @Groups({"apiV1"})
     *
     * @var int
     */
    private $countOfNeos;

    public function __construct(int $month, int $countOfNeos)
    {
        if ($month <= 0 || $month >= 13) {
            throw new \InvalidArgumentException('Invalid range for month attr');
        }
        $this->month = $month;

        if ($countOfNeos < 0) {
            throw new \InvalidArgumentException('Invalid range for countOfNeos attr');
        }
        $this->countOfNeos = $countOfNeos;
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @return int
     */
    public function getCountOfNeos(): int
    {
        return $this->countOfNeos;
    }
}
