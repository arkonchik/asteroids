<?php

declare(strict_types=1);

namespace App\DTO\Nasa;

class Neo
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $referenceId;
    /**
     * @var string
     */
    private $name;
    /**
     * @var float
     */
    private $kilometersPerHour;
    /**
     * @var bool
     */
    private $isPotentiallyHazardousAsteroid;
    /**
     * @var \DateTimeImmutable
     */
    private $closeApproachDate;

    public function __construct(
        int $referenceId,
        string $name,
        float $kilometersPerHour,
        bool $isPotentiallyHazardousAsteroid,
        \DateTimeImmutable $closeApproachDate
    ) {
        if ($referenceId <= 0) {
            throw new \InvalidArgumentException('referenceId must be positive');
        }
        if (0 === strlen($name)) {
            throw new \InvalidArgumentException('name must be not empty string');
        }
        if ($kilometersPerHour <= 0) {
            throw new \InvalidArgumentException('kilometersPerHour must be positive');
        }

        $this->referenceId = $referenceId;
        $this->name = $name;
        $this->kilometersPerHour = $kilometersPerHour;
        $this->isPotentiallyHazardousAsteroid = $isPotentiallyHazardousAsteroid;
        $this->closeApproachDate = $closeApproachDate;
    }

    /**
     * @return int
     */
    public function getReferenceId(): int
    {
        return $this->referenceId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getKilometersPerHour(): float
    {
        return $this->kilometersPerHour;
    }

    /**
     * @return bool
     */
    public function isPotentiallyHazardousAsteroid(): bool
    {
        return $this->isPotentiallyHazardousAsteroid;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCloseApproachDate(): \DateTimeImmutable
    {
        return $this->closeApproachDate;
    }
}
