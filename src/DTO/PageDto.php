<?php

declare(strict_types=1);

namespace App\DTO;

class PageDto
{
    /**
     * @var int
     */
    private $page;
    /**
     * @var int
     */
    private $perPage;

    public function __construct(int $page = 1, int $perPage = 20)
    {
        $this->page = $page;
        $this->perPage = $perPage;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->page * $this->perPage;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return ($this->page - 1) * $this->perPage;
    }
}
