<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NeoRepository")
 * @ORM\Table(name="neo", indexes={
 *     @ORM\Index(name="speed", columns={"speed"}),
 *     @ORM\Index(name="is_hazardous", columns={"is_hazardous"}),
 * })
 */
class Neo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     * @Groups({"apiV1"})
     *
     * @var int
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"apiV1"})
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"apiV1"})
     *
     * @var bool
     */
    private $isHazardous;

    /**
     * @ORM\Column(type="date_immutable")
     * @Groups({"apiV1"})
     *
     * @var \DateTimeImmutable
     */
    private $date;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=11)
     * @Groups({"apiV1"})
     *
     * @var float
     */
    private $speed;

    public function __construct(
        int $reference,
        \DateTimeImmutable $date,
        string $name,
        float $speed,
        bool $isHazardous
    ) {
        $this->reference = $reference;
        $this->date = $date;
        $this->name = $name;
        $this->isHazardous = $isHazardous;
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getReference(): int
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isHazardous(): bool
    {
        return $this->isHazardous;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getSpeed(): float
    {
        return $this->speed;
    }
}
