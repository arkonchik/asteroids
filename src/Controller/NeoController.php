<?php

namespace App\Controller;

use App\DTO\PageDto;
use App\Repository\NeoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NeoController extends AbstractController
{
    /**
     * @var NeoRepository
     */
    private $neoRepository;

    public function __construct(NeoRepository $neoRepository)
    {
        $this->neoRepository = $neoRepository;
    }

    /**
     * @Route("/neo/best-month")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function bestMonth(Request $request)
    {
        $hazardous = $request->query->getBoolean('hazardous', false);
        $result = $this->neoRepository->findMonthWithMostAsteroids($hazardous);

        return $this->jsonWithDefaultContext($result);
    }

    /**
     * @Route("/neo/hazardous")
     *
     * @param PageDto $pageDto
     *
     * @return JsonResponse
     */
    public function listHazardous(PageDto $pageDto)
    {
        $neoList = $this->neoRepository->findByFilter(true, $pageDto);

        return $this->jsonWithDefaultContext($neoList);
    }

    /**
     * @Route("/neo/fastest")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function fastest(Request $request)
    {
        $hazardous = $request->query->getBoolean('hazardous', false);
        $neo = $this->neoRepository->findFastest($hazardous);

        return $this->jsonWithDefaultContext($neo);
    }

    /**
     * @param       $data
     * @param int   $status
     * @param array $headers
     * @param array $context
     *
     * @return JsonResponse
     */
    private function jsonWithDefaultContext($data, int $status = 200, array $headers = [], array $context = ['groups' => ['apiV1']]): JsonResponse
    {
        return $this->json($data, $status, $headers, $context);
    }
}
