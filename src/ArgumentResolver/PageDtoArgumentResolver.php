<?php

declare(strict_types=1);

namespace App\ArgumentResolver;

use App\DTO\PageDto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class PageDtoArgumentResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return PageDto::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $pageDto = new PageDto(
            (int) $request->query->get('page', 1),
            (int) $request->query->get('perPage', 20)
        );

        yield $pageDto;
    }
}
