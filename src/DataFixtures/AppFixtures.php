<?php

namespace App\DataFixtures;

use App\Entity\Neo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $data = [
            [
                'date' => '2019-01-02',
                'reference' => 1,
                'name' => 'name1',
                'speed' => 22221.111,
                'isHazardous' => true,
            ],
            [
                'date' => '2019-03-01',
                'reference' => 2,
                'name' => 'name2',
                'speed' => 1000.222,
                'isHazardous' => false,
            ],
            [
                'date' => '2019-03-02',
                'reference' => 3,
                'name' => 'name3',
                'speed' => 999999.111,
                'isHazardous' => true,
            ],
            [
                'date' => '2019-03-05',
                'reference' => 4,
                'name' => 'name3',
                'speed' => 9.111,
                'isHazardous' => true,
            ],
        ];
        foreach ($data as $item) {
            $neo = new Neo(
                $item['reference'],
                new \DateTimeImmutable($item['date']),
                $item['name'],
                $item['speed'],
                $item['isHazardous']
            );
            $manager->persist($neo);
        }

        $manager->flush();
    }
}
