<?php

namespace App\Repository;

use App\DTO\BestMonthResult;
use App\DTO\PageDto;
use App\Entity\Neo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Neo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Neo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Neo[]    findAll()
 * @method Neo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NeoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Neo::class);
    }

    /**
     * @param int ...$references
     *
     * @return Neo[]
     */
    public function findByReferences(int ...$references): array
    {
        $result = $this->findBy(['reference' => $references]);

        return $result;
    }

    /**
     * @param bool    $isHazardous
     * @param PageDto $pageDto
     *
     * @return Neo[]
     */
    public function findByFilter(bool $isHazardous, PageDto $pageDto): array
    {
        $query = $this->createQueryBuilder('n')
            ->andWhere('n.isHazardous = :isHazardous')
            ->setParameter('isHazardous', $isHazardous)
            ->setMaxResults($pageDto->getLimit())
            ->setFirstResult($pageDto->getOffset())
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @param bool $isHazardous
     *
     * @return Neo|null
     */
    public function findFastest(bool $isHazardous): ?Neo
    {
        $query = $this->createQueryBuilder('n')
            ->andWhere('n.isHazardous = :isHazardous')
            ->setParameter('isHazardous', $isHazardous)
            ->orderBy('n.speed', 'DESC')
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param bool $isHazardous
     *
     * @return BestMonthResult|null
     */
    public function findMonthWithMostAsteroids(bool $isHazardous): ?BestMonthResult
    {
        //can perform operation of this query
        //or aggregate this data MONTH(date) in separate field while saving
        //or use result cache and invalidate cache while adding new data
        $dql = 'SELECT 
                    MONTH(n.date) as mostNeoMonth,
                    count(1) as countOfNeo 
                FROM App\Entity\Neo n
                WHERE n.isHazardous = :isHazardous
                GROUP by mostNeoMonth
                ORDER by countOfNeo desc';
        $data = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('isHazardous', $isHazardous)
            ->setMaxResults(1)
            ->getScalarResult();

        return (!empty($data[0]))
            ? new BestMonthResult($data[0]['mostNeoMonth'], $data[0]['countOfNeo'])
            : null;
    }

    /**
     * @param Neo ...$neos
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Neo ...$neos)
    {
        if (empty($neos)) {
            throw new \InvalidArgumentException('Need to pass not empty neos');
        }

        $em = $this->getEntityManager();
        foreach ($neos as $neo) {
            $em->persist($neo);
        }
        $em->flush();
    }
}
