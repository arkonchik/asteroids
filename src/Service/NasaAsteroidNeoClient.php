<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\Nasa\Neo;
use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Psr\Http\Message\RequestInterface;
use function GuzzleHttp\Psr7\build_query;

class NasaAsteroidNeoClient
{
    /**
     * @var HttpClient
     */
    private $client;
    /**
     * @var MessageFactory
     */
    private $messageFactory;
    /**
     * @var string
     */
    private $apiKey;

    public function __construct(HttpClient $client, MessageFactory $messageFactory, string $apiKey)
    {
        $this->client = $client;
        $this->messageFactory = $messageFactory;
        $this->apiKey = $apiKey;
    }

    /**
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     *
     * @return Neo[]
     *
     * @throws \Http\Client\Exception
     */
    public function retrieve(\DateTimeInterface $startDate, \DateTimeInterface $endDate): array
    {
        $request = $this->buildRequest($startDate, $endDate);
        $response = $this->client->sendRequest($request);

        $content = $response->getBody()->getContents();
        $data = \GuzzleHttp\json_decode($content, true);

        $neoDataCollection = [];
        if (isset($data['near_earth_objects'])) {
            $neoDataCollection = $this->hydrateNeoCollection($data, $neoDataCollection);
        }

        return $neoDataCollection;
    }

    /**
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     *
     * @return RequestInterface
     */
    private function buildRequest(
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate
    ): RequestInterface {
        $params = [
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d'),
            'api_key' => $this->apiKey,
        ];
        $url = '/neo/rest/v1/feed?'.build_query($params);
        $request = $this->messageFactory->createRequest('GET', $url);

        return $request;
    }

    /**
     * @param array $data
     * @param array $neoDataCollection
     *
     * @return array
     *
     * @throws \Exception
     */
    private function hydrateNeoCollection($data, array $neoDataCollection): array
    {
        foreach ($data['near_earth_objects'] as $neosData) {
            foreach ($neosData as $neoData) {
                $firstCloseApproachData = reset($neoData['close_approach_data']);
                if (!isset($neoData['is_potentially_hazardous_asteroid'])) {
                    throw new \InvalidArgumentException('No key "is_potentially_hazardous_asteroid" exists');
                }

                $neoDataCollection[] = new Neo(
                    (int) $neoData['neo_reference_id'],
                    $neoData['name'],
                    (float) $firstCloseApproachData['relative_velocity']['kilometers_per_hour'],
                    (bool) $neoData['is_potentially_hazardous_asteroid'],
                    new \DateTimeImmutable($firstCloseApproachData['close_approach_date'])
                );
            }
        }

        return $neoDataCollection;
    }
}
