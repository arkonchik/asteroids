<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Neo;
use App\Repository\NeoRepository;

class NeoImporter
{
    /**
     * @var NasaAsteroidNeoClient
     */
    private $nasaAsteroidNeoClient;
    /**
     * @var NeoRepository
     */
    private $neoRepository;

    public function __construct(NasaAsteroidNeoClient $nasaAsteroidNeoClient, NeoRepository $neoRepository)
    {
        $this->nasaAsteroidNeoClient = $nasaAsteroidNeoClient;
        $this->neoRepository = $neoRepository;
    }

    /**
     * @param \DateTimeImmutable $startDate
     * @param \DateTimeImmutable $endDate
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Http\Client\Exception
     */
    public function import(\DateTimeImmutable $startDate, \DateTimeImmutable $endDate)
    {
        $nasaNeoResult = $this->nasaAsteroidNeoClient->retrieve($startDate, $endDate);
        $references = $this->retrieveReferences($nasaNeoResult);
        $existingNeos = $this->neoRepository->findByReferences(...$references);
        $existingNeoReferences = $this->retriveExistingRefrences($existingNeos);

        $neosNeedToSave = [];
        foreach ($nasaNeoResult as $neo) {
            if (!in_array($neo->getReferenceId(), $existingNeoReferences, true)) {
                $neosNeedToSave[] = $this->hydrateClientNeoObjectToEntity($neo);
            }
        }

        if (!empty($neosNeedToSave)) {
            $this->neoRepository->save(...$neosNeedToSave);
        }
    }

    /**
     * @param $nasaNeoResult
     *
     * @return array
     */
    private function retrieveReferences($nasaNeoResult): array
    {
        $references = [];
        foreach ($nasaNeoResult as $item) {
            $references[] = $item->getReferenceId();
        }

        return $references;
    }

    /**
     * @param array $existingNeos
     *
     * @return array
     */
    private function retriveExistingRefrences(array $existingNeos): array
    {
        $existingNeoReferences = [];
        foreach ($existingNeos as $existingNeo) {
            $existingNeoReferences[] = $existingNeo->getReference();
        }

        return $existingNeoReferences;
    }

    /**
     * @param \App\DTO\Nasa\Neo $neo
     *
     * @return array
     */
    private function hydrateClientNeoObjectToEntity(\App\DTO\Nasa\Neo $neo): array
    {
        return new Neo(
            $neo->getReferenceId(),
            $neo->getCloseApproachDate(),
            $neo->getName(),
            $neo->getKilometersPerHour(),
            $neo->isPotentiallyHazardousAsteroid()
        );
    }
}
