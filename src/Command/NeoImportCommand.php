<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\NeoImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class NeoImportCommand extends Command
{
    private const MAX_DAYS_IMPORT_INTERVAL = 3;

    /**
     * @var NeoImporter
     */
    private $neoImporter;

    public function __construct(string $name = null, NeoImporter $neoImporter)
    {
        parent::__construct($name);
        $this->neoImporter = $neoImporter;
    }

    protected function configure()
    {
        $this
            ->setName('neo:import')
            ->setDescription('Retrieve neo\'s from nasa api and save unique in DB.')
            ->addOption('startDate', null, InputOption::VALUE_REQUIRED, 'Start date.')
            ->addOption('endDate', null, InputOption::VALUE_REQUIRED, 'End date.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start importing nasa neo data');

        $startDate = new \DateTimeImmutable($input->getOption('startDate'));
        $endDate = new \DateTimeImmutable($input->getOption('endDate'));
        $interval = $startDate->diff($endDate, true);
        if ($interval->days > self::MAX_DAYS_IMPORT_INTERVAL) {
            $output->writeln(sprintf('Interval of days cannot be more than %d', self::MAX_DAYS_IMPORT_INTERVAL));

            return;
        }

        $this->neoImporter->import($startDate, $endDate);

        $output->writeln('Finish importing nasa neo data');
    }
}
