First of all:
- Install env
```
docker-compose up -d
```

- Run migrations
```
docker-compose exec php-fpm bin/console doctrine:migrations:migrate
```

After that

Run import nasa neo's
```
docker-compose exec php-fpm bin/console neo:import --startDate=2015-01-27 --endDate=2015-01-30
```

Run tests
```
docker-compose exec php-fpm bin/phpunit tests
```

Check cs
```
docker-compose exec php-fpm ./vendor/bin/php-cs-fixer fix --dry-run
```