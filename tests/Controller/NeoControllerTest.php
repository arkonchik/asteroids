<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NeoControllerTest extends WebTestCase
{
    public function testNeoListSuccessResponse()
    {
        $client = self::createClient([]);
        $client->request('GET', '/neo/hazardous?page=2&perPage=2');
        $response = $client->getResponse();

        $this->assertResponseStatusCodeSame(200);
        $resultData = \GuzzleHttp\json_decode($response->getContent(), true);
        $this->assertNotEmpty($resultData);

        foreach ($resultData as $item) {
            $this->assertValidNeoStructure($item);
        }
    }

    public function testNeoFastestSuccessResponse()
    {
        $client = self::createClient([]);
        $client->request('GET', '/neo/fastest?hazardous=true');

        $this->assertResponseStatusCodeSame(200);
        $response = $client->getResponse();
        $resultData = \GuzzleHttp\json_decode($response->getContent(), true);

        $this->assertValidNeoStructure($resultData);
    }

    public function testNeoBestMonthSuccessResponse()
    {
        $client = self::createClient([]);
        $client->request('GET', '/neo/best-month?hazardous=false');
        $response = $client->getResponse();
        $this->assertResponseStatusCodeSame(200);
        $resultData = \GuzzleHttp\json_decode($response->getContent(), true);

        $this->assertArrayHasKey('month', $resultData);
        $this->assertArrayHasKey('countOfNeos', $resultData);
    }

    /**
     * @param $item
     */
    private function assertValidNeoStructure($item): void
    {
        $this->assertIsArray($item);

        $this->assertArrayHasKey('date', $item);
        $this->assertArrayHasKey('reference', $item);
        $this->assertArrayHasKey('name', $item);
        $this->assertArrayHasKey('speed', $item);
        $this->assertArrayHasKey('isHazardous', $item);
    }
}
