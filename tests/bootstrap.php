#!/usr/bin/env php
<?php

require dirname(__DIR__).'/config/bootstrap.php';

(new \Symfony\Component\Dotenv\Dotenv(true))->load(__DIR__.'/../.env.test');

if (isset($_ENV['BOOTSTRAP_CLEAR_CACHE_ENV'])) {
    // executes the "php bin/console cache:clear" command
    passthru(sprintf(
                 'php "%s/../bin/console" cache:clear --env=%s --no-warmup',
                 __DIR__,
                 $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']
             ));
}

echo shell_exec(sprintf(
                'php %s/../bin/console doctrine:migrations:migrate --no-interaction --env=%s',
                __DIR__,
                $_ENV['APP_ENV']
                )
);

echo shell_exec(sprintf(
                    'php %s/../bin/console doctrine:fixtures:load --no-interaction --env=%s',
                    __DIR__,
                    $_ENV['APP_ENV']
                )
);
